import re

brojevi = []
brojac = 0
ukupno = 0
while True:
    unos = input("Ime datoteke: ")
    if(unos == 'mbox-short.txt'):
        fhand = open('mbox-short.txt')
        break
    elif(unos == 'mbox.txt'):
        fhand = open('mbox.txt')
        break

for line in fhand:
    line = line.rstrip()
    if(line.startswith('X-DSPAM-Confidence:')):
        ukupno += float(line.split(':', 1)[1])
        brojac += 1

print("Average X-DSPAM-Confidence: ", ukupno/brojac)