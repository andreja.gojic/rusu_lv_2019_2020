brojevi = [None] * 1000
brojac = 0

while True:
    try:
        unos = input("Unesite broj: ")
        if(unos == 'Done'):
            break
        else:
            brojevi[brojac] = float(unos)
            brojac +=1
    except ValueError:
        print("Neispravan unos!")

min = brojevi[0]
max = brojevi[0]
i = 0
ukupno = 0
while i < brojac:
    ukupno += brojevi[i]
    if(brojevi[i] != None and brojevi[i] > max):
        max = brojevi[i]
    if(brojevi[i] != None and brojevi[i] < min):
        min = brojevi[i]
    i += 1
   
print("Unijeli ste ", brojac, " brojeva")
print("Srednja vrijednost unesenih brojeva je ", ukupno/brojac)
print("Minimalna vrijednost unosa je: ", min)
print("Maksimalna vrijednost unosa je: ", max)