while True:
    try:
        broj = float(input("Unesite broj: "))
        if(broj < 0.0 or broj > 1.0):
            print("Broj je izvan intervala!")
        else:
            break
    except ValueError:
        print("Neispravan unos!")
       
if(broj <= 1.0 and broj >= 0.9):
    print("Broju ", broj, " pripada ocjena A")
elif(broj < 0.9 and broj >=0.8):
    print("Broju ", broj, " pripada ocjena B")
elif(broj < 0.8 and broj >=0.7):
    print("Broju ", broj, " pripada ocjena C")
elif(broj < 0.7 and broj >=0.6):
    print("Broju ", broj, " pripada ocjena D")
else:
    print("Broju ", broj, " pripada ocjena F")