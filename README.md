# Raspoznavanje uzoraka i strojno učenje

Ak.godina 2019./2020.


## Laboratorijske vježbe

Ovaj repozitorij sadrži potrebne datoteke za izradu svake laboratorijske vježbe (npr. LV1/resources). Rješenje svake laboratorijske vježbe treba spremiti u odgovarajući direktorij (npr. LV1/solutions).


## Podaci o studentu:

Ime i prezime:
Andreja Gojic


## Izmjena u counting_words.py

Prvu naredbu "fname = raw_input('Enter the file name: ')" sam izmijenila tako sto sam umjesto raw_input napisala samo input, zatim sam u naredbi kada se otvara file "fhand = open(fnamex)" obrisala 'x' kod fnamex te sam u except-u obrisala "exit()" i u print-u dodala zagrade pa on izgleda " print('File cannot be opened:', fname)". U drugoj for petlji u uvjetu if se provjerava je li word unutar counts te ukoliko nije, counts-u s indeksom word se dodjeljuje vrijednost 1, a ukoliko je, vrijednost se uvecava za 1. Na kraju sam prilikom ispisivanja dictionary-a dodala zagrade pa ta naredba izgleda "print(counts)".

## Opis LV1

U prvom zadatku bilo je potrebno ucitati s tipkovnice broj odradenih sati te placu po odradenom satu, zatim se izracunava ukupna placa i ispisuje. u drugom zadatku se ucitava broj s tipkovnice koji mora biti u intervalu 0.0-1.0, ukoliko je broj izvan zadanog intervala program izbacuje except s kratkom porukom, a ukoliko je broj zadovoljavajuc onda se provjerava kojoj ocjeni A-F pripada. Treci zadatak je identican kao i prvi samo sto se izracun place obavlja pomocu funkcije. U petom zadatku se putem tipkovnice unose proizvoljni brojevi te kad se pojavi unos "Done" program ispisuje srednju vrijjednost svih unesenih brojevi, najvecu te najmanju vrijednost, a ukoliko unos nije broj niti "Done" program izbacuje except s porukom. U petom zadatku se unosi naziv datoteke te ako program prepozna naziv datoteke ona se otvara i racuna se srednja vrijednost X-DSPAM-Confidence koja se potom ispisuje, a ukoliko program ne prepozna unos trazi novi unos sve dok mu ne bude poznat. U sestom zadatku se ucitavaju sve email adrese iz datoteke mbox-short.txt, zatim se email adrese rastavljaju na domene koje se spremaju u dictionary i ukoliko se takva domena vec nalazi u dictionary-u njen broj se povecava za jedan, a ukoliko se ne nalazi pridijeli joj se vrijednost 1 te se na kraju na ekran ispisuju domene s brojem ponavljanja.

## Opis LV2

U prvom zadatku se ucita datoteka mbox-short.txt, zatim se iz nje izvdvoje sve email adrese, a iz email adresa se izdvoji prvi dio do znaka @ te se ispise. Drugi zadatak se nadovezuje na prvi te se provjerava koliko ima email adrese koje zadovoljavaju navedene uvjete. U trecem zadatku se generira vektor s nulama i jedinicama, nule predstvaljaju zene, a jedinice muskarce, nakon toga se generira drugi vektor koji sadrzi nasumicne vrijednosti visine za pojedinu zenu, odnosno muskarca te se pomocu np.scatter prikazu rezultati gdje su crvenom bojom oznacene zene, a plavom muskarci, dok je crni x-evima oznacena srednja vrijednost visine za pojedini spol. U cetvrtom zadatku se generira 100 nasumicnih bacanja igrace kockice te su rezultati prikazani histogramom. U petom zadatku je pomocu pd.read_csv ucitan csv dokument s podatci o vozilima te su ti podatci prikazani s np.scatter. U sestom zadatku se ucita slika tiger.png te se svaki piksel pomnozi s 3 te se na taj nacin dobije slika koja je posvjetljena u odnosu na originalnu.