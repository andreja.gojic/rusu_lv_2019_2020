import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

#%%
# url that contains valid xml file:
url = "http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2017&vrijemeDo=01.01.2018"

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = list(list(root))[i]
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme')
plt.figure()
# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek



max_koncentracija = df.sort_values(by = 'mjerenje', ascending = False)
print(max_koncentracija.head(3)['vrijeme'].dt.date)


missing = [31,28,31,30,31,30,31,31,30,31,30,31] - df.groupby('month').mjerenje.count()
missing.plot.bar()



usporedba_ljetaZime = df[(df.month == 2) | (df.month == 7)]
usporedba_ljetaZime.boxplot(column=['mjerenje'], by = ['month'])


df['isWeekend'] = (df['dayOfweek'] == 5) | (df['dayOfweek'] == 6)
df.boxplot(column=['mjerenje'], by=['isWeekend'])
