import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv("../resources/mtcars.csv")

cyl_cars = mtcars.groupby('cyl')
cyl_cars_mean = cyl_cars.mean()
cyl_cars_mean.plot.bar(y='mpg')


cyl4 = mtcars[(mtcars.cyl == 4)]
cyl6 = mtcars[(mtcars.cyl == 6)]
cyl8 = mtcars[(mtcars.cyl == 8)]

plt.figure()
plt.boxplot([cyl4.wt, cyl6.wt, cyl8.wt], positions = [4, 6, 8])
plt.xlabel('cyl')
plt.ylabel('wt')
plt.grid()

am = mtcars.groupby('am')
am = am.mean()
am.plot.bar(y = 'mpg')

automatic = mtcars[(mtcars.am == 0)]
manual = mtcars[(mtcars.am == 1)]

plt.figure()
plt.scatter(automatic.qsec, automatic.hp, marker='o')   
plt.scatter(manual.qsec, manual.hp, marker='s')  
plt.ylabel('Snaga')
plt.xlabel('Ubrzanje')
plt.legend(["automatic","manual"])
plt.grid()













