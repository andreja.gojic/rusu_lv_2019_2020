import pandas as pd



mtcars = pd.read_csv('../resources/mtcars.csv')

max_potrosnja= mtcars.sort_values(by=['mpg'], ascending=False)
print("5 automobila s najvecom potrosnjom:")
print(max_potrosnja.head(5))

min_potrosnja = mtcars.sort_values(by = ['cyl'], ascending = True)
min_potrosnja = min_potrosnja.query('cyl == 8')
print("3 automobila s 8 cilindara s najmanjom potrosnjom: ", min_potrosnja.head(3))

sest_cilindara = mtcars.query('cyl == 6')
print("Srednja potrosnja automobila sa 6 cilindara: ", sest_cilindara['mpg'].mean())

cetiri_cilindra = mtcars.query('cyl==4')
cetiri_cilindra = cetiri_cilindra.query('wt <= 2.2 & wt >= 2')
print("Srednja potrosnja automobila s 4 cilindra: ", cetiri_cilindra['mpg'].mean())

automatski = mtcars.query('am==0')
print("Automatski mjenjac: ", automatski['car'].count())
print("Rucni mjenajc: ", mtcars['car'].count()-automatski['car'].count())

automatski = automatski.query('hp > 100')
print("Automatski mjenjaci s preko 100 konjskih snaga: ", automatski['car'].count())

masa_kg = mtcars.wt * 453.59237
auti_kg = pd.DataFrame(mtcars, columns=['car'])
auti_kg['wt in kg'] = masa_kg
print(auti_kg)


