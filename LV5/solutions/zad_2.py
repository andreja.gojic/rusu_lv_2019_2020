from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
import sklearn.cluster as clstr

def generate_data(n_samples, flagc):
 if flagc == 1:
    random_state = 365
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
 elif flagc == 2:
    random_state = 148
    x,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state) 
    transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
    x = np.dot(x, transformation)
 elif flagc == 3:
    random_state = 148
    x, y = datasets.make_blobs(n_samples=n_samples, centers = 4, cluster_std=[1.0, 2.5, 0.5, 3.0],random_state=random_state) 
 elif flagc == 4:
    x, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
 elif flagc == 5:
    x, y = datasets.make_moons(n_samples=n_samples, noise=.05)
 else:
    x = []

 return x

clusters = np.arange(2, 21, 1) 
gen_datas = np.arange(1, 6, 1) 
colors = ['darkmagenta', 'rebeccapurple', 'navy', 'salmon', 'blueviolet']
for gen_data in gen_datas:
    data = generate_data(500, gen_data)
    inertia_s = []
    for cluster in clusters:
        kmeans = clstr.KMeans(cluster).fit(data)
        inertia_s.append(kmeans.inertia_) 
    plt.figure(gen_data)
    plt.plot(clusters, inertia_s, c = colors[gen_data-1])
    plt.xlabel('Broj clustera')
    plt.ylabel('Kriterijska f-ja')
    plt.title('Graf za skupinu podataka ' + str(gen_data))
plt.legend()
plt.show()
