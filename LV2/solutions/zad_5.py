import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

cars = pd.read_csv('mtcars.csv')

plt.scatter(cars.hp, cars.mpg, c = cars.wt, cmap = 'copper')
plt.colorbar().set_label('wt')
plt.xlabel('hp')
plt.ylabel('mpg')

print('Minimalna vrijednost potrosnje: ')
print(min(cars.mpg))
print('Maksimalna vrijednost potrosnje: ')
print(max(cars.mpg))
print('Prosjecna vrijednost potrosnje: ')
print(np.average(cars.mpg))