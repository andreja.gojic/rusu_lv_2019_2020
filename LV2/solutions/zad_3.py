import numpy as np
import matplotlib.pyplot as plt


def srVrijednosti():
    sumaVisina_m = np.zeros([])
    np.dot(spol, visina_uk, sumaVisina_m)
    sumaVisina_z = visina_uk.sum() - sumaVisina_m
    srVrijednost_m = sumaVisina_m/spol.sum()
    srVrijednost_z = sumaVisina_z/(n - spol.sum())
    return [srVrijednost_m, srVrijednost_z]

n = 50
spol = np.random.randint(0,2, size = n)
visina_uk =np.zeros(n)
visina_m = []
visina_z = []
j=0
k=0
for i in range(n):
    if spol[i] == 1:
        visina_uk[i] = np.random.normal(180, 7)
    else:
        visina_uk[i] = np.random.normal(167, 7)
       
color = np.array(['r','b'])
plt.scatter(spol, visina_uk, marker = '.', c = color[spol])
srednjeVrijednosti = srVrijednosti()
spolovi = np.array([1, 0])
plt.scatter(spolovi, srednjeVrijednosti, marker = 'x', c = 'black')
