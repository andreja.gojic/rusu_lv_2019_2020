import re

file = open('mbox-short.txt')
minJedanA =0
jedanA =0
bezA =0
znamenke =0
malaSlova=0
najmanjeJednoA=[]
tocnoJedanA=[]
nemaA=[]
brojevi=[]
samoMalaSlova=[]
for line in file:
    email = re.findall('\S+@\S+', line)
    if(email):
        index = email[0].index('@')
        if(email[0][0]=='<'):
            name=email[0][1:index]
        else:
            name=email[0][:index]
        if(name):
            if name.count('a') >= 1:
                najmanjeJednoA.append(name)
                minJedanA +=1
            if name.count('a') == 1:
                tocnoJedanA.append(name)
                jedanA += 1
            if re.match('[^a]*', name):
                nemaA.append(name)
                bezA += 1
            if re.match('.*[0-9]+.*', name):
                brojevi.append(name)
                znamenke += 1
            if re.match('^[a-z]+$',name):
                samoMalaSlova.append(name)
                malaSlova += 1
print('Broj adresa koje sadrže najmanje jedno slovo a:')
print(minJedanA)
print('Broj adresa koje sadrže točno jedno slovo a:')
print(jedanA)
print('Beoj adresa koje ne sadrže slovo a:')
print(bezA)
print('Broj adresa koje sadrže jedan ili više numeričkih znakova (0 – 9):')
print(znamenke)
print('Broj adresa koje sadrže samo mala slova (a-z): ')
print(malaSlova)